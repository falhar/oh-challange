const readline=require("readline");
const rl=readline.createInterface({
    input:process.stdin,
    output:process.stdout
});

function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null;
}

function testAgain (wish) {
    rl.question("Wanna test another size?[y/n]", wish=>{
        if (wish=="y") {
            testSize();
        } else if (wish=="n") {
            process.exit();
        } else {
            console.log("only input y or n");
            testAgain();
        }
    })
}

function testSize(num) {
    rl.question("Your size : ", num=>{
        if (!isNaN(num)&&!isEmptyOrSpaces(num)) {
            if (num<0) {
                console.log("Size must be more than 0");
                testAgain();
            } else {
                if (num<5) {
                    console.log("Tiny");
                    testAgain();
                } else if (num<10) {
                    console.log("Small");
                    testAgain();
                } else if (num<15) {
                    console.log("Medium");
                    testAgain();
                } else if (num<20) {
                    console.log("Large");
                    testAgain();
                } else {
                    console.log("Huge");
                    testAgain();
                }
            }
        } else {
            console.log("Input must be a number");
            testSize();
        }
    })
}
testSize();