const readline=require("readline");
const rl=readline.createInterface({
    input:process.stdin,
    output:process.stdout
});

function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null;
}

function testAgain (wish) {
    rl.question("Wanna test another score?[y/n]", wish=>{
        if (wish=="y") {
            testScore();
        } else if (wish=="n") {
            process.exit();
        } else {
            console.log("only input y or n");
            testAgain();
        }
    })
}
function testScore(score) {
    rl.question("Your score : ", score=>{
        if (!isNaN(score)&&!isEmptyOrSpaces(score)) {
            if (score<0||score>100) {
                console.log("Invalid score");
                testAgain();
            } else {
                if (score>60) {
                    console.log("Passed");
                    testAgain();
                } else {
                    console.log("Failed");
                    testAgain();
                }
            }
        } else {
            console.log("Input must be a number");
            testScore();
        }
    })
}
testScore();