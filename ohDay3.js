const readline=require("readline");
const rl=readline.createInterface({
    input:process.stdin,
    output:process.stdout
});

function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null;
}

function testAgain (wish) {
    rl.question("Want to try again?[y/n]", wish=>{
        if (wish=="y") {
            multiply();
        } else if (wish=="n") {
            console.clear();
            process.exit();
        } else {
            console.log("Only input y or n");
            testAgain();
        }
    })
}

function multiply(num1, num2) {
    console.clear();
    console.log("Program to multiply the inputs");
    console.log("==============================");
    rl.question("input 1 : ", num1=>{
        rl.question("input 2 : ", num2=>{
            if (!isEmptyOrSpaces(num1)&&!isEmptyOrSpaces(num2)) {
                if (!isNaN(num1)&&!isNaN(num2)) {
                    console.log(`result = ${num1*num2}`);
                    testAgain();
                } else {
                    console.log("Please provide a number only!");
                    testAgain();
                }
            } else {
                console.log("Please provide a value!");
                testAgain();
            }
        })
    })
}
multiply();