function caseInSwitch(val) {
    switch (val) {
        case 1:
            console.log("alpha");
            break;
        case 2:
            console.log("beta");
            break;
        case 3:
            console.log("gamma");
            break;
        case 4:
            console.log("delta");
            break;
        default :
            console.log("Input must be 1-4");
    }
}
caseInSwitch(1);
caseInSwitch(2);
caseInSwitch(3);
caseInSwitch(4);