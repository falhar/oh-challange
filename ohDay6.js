function multiplyAll(arr) {
  var product = 1;
  // Only change code below this line
  for (i=0;i<arr.length;i++) {
      let multi=[]
      multi.push(arr[i].reduce((a,b)=>a*b))
      product*=multi.reduce((a,b)=>a*b)
  }
  // Only change code above this line
  return product;
}

console.log(multiplyAll([[1,2],[3,4],[5,6,7]]));
//multiplyAll([[1,2],[3,4],[5,6,7]]) should return 5040
